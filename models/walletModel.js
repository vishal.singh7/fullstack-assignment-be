import mongoose from "mongoose";

// wallet schema
let walletSchema = new mongoose.Schema({
    balance: {type: String, required: true},
    transactionId: {type: String, required: true},
    name: {type: String, required: true},
    date: {type: Date, required: true}
},
);

// credit/debit schema
let creditdebitSchema = new mongoose.Schema({
    object_id: { type: mongoose.Types.ObjectId , ref: 'Wallet'},
    amount: {type: String, required: true},
    description: {type: String, required: true},
})


const Wallet = mongoose.model("Wallet", walletSchema);
const CreditWallet = mongoose.model("CreditWallet", creditdebitSchema )

module.exports = {
    Wallet , 
    CreditWallet
};
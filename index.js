import express from "express";
require("./config/db");
const app = express();
import cors from "cors";
import bodyParser from "body-parser";

import WalletRouter from "./routes/walletRoute";


// ALL THE MIDDLEWARES
app.use(express.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));

// ALL ROUTES
app.use("/api", WalletRouter);


app.listen(5000, () => {
  console.log("Server is running");
});

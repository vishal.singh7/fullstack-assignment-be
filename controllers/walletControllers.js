import {Wallet, CreditWallet} from "../models/walletModel";
// import CreditWallet from "../models/walletModel";

// post creat wallet
const createWallet = async (req, res, next) => {
  try {
    const {balance, transactionId, name } = req.body;
    if (!balance || !transactionId || !name) {
      res.status(400).json({error:{message: 'All fields are required'}})
    } else {
      // create the wallet
      const doc = new Wallet({
        balance,
        transactionId,
        name,
        date : new Date()
      });
      // save the wallet details
      const walletsave = await doc.save();
      if (walletsave?._id) {
        res.status(200).json({id:doc._id, balance:doc.balance, transactionId:doc.transactionId, name:doc.name, date:doc.date})
      } else {
        res.status(400).json({ message: "wallet not saved" });
      }
    }
  } catch (error) {
    next(error)
  }
}



// post credit wallet
const creditWallet = async (req, res, next) => {
  try {
    const {amount, description } = req.body;
    if (!amount || !description) {
      res.status(400).json({error:{message: 'All fields are required'}})
    } else {
      const findid = await Wallet.findOne({_id: req.params.walletId})
      // create the wallet
      const doc = new CreditWallet({
        amount,
        description,
        object_id: req.params.walletId,
      });
      // save the wallet details
      const walletsave = await doc.save();
      if (walletsave?._id) {
        return res.status(200).json({balance:findid.balance, transactionId:findid.transactionId})
      } else {
        res.status(400).json({ message: "wallet not credit" });
      }
    }
  } catch (error) {
    next(error)
  }
}



// det wallet by id
const getwalletbyid = async (req, res, next) => {
  try {
    let data = await Wallet.findOne({_id:req.params.Id});
    return res.json({ data });
  } catch (error) {
    next(error);
  }
};




module.exports = { createWallet,
  creditWallet,
  getwalletbyid,
 };

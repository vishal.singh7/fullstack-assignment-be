import express from 'express'
import WalletControllers from "../controllers/walletControllers";
const router = express.Router();

// create wallet
router.post("/setup", WalletControllers.createWallet);

// create credit wallet
router.post("/transact/:walletId", WalletControllers.creditWallet);


//  show wallets by id
router.get("/getwalletbyid/:Id", WalletControllers.getwalletbyid);


module.exports = router;
